#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
"""

# %% Package
import numpy as np

# %% Main Function


def monthly_date(date):
    """
    Get year and month from a datestamp as a integer
    """
    mdate = date.year*100+date.month

    return mdate

