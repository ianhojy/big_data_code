#!/usr/bin/env /shared/share_dff/anaconda3/bin/python
# coding: utf-8

""""
# * Program          : loading_sorted_portfolio_allocations.py
# * Author           : Kent Daniel, Lira Mota, Simon Rottke and Tano Santos
# * Date             : 2020-01
# * Description      : calculates DMRS portfolio breakpoints and allocations.
# * Dependencies     : stock_annual, beta_tables.
# * Output Tables    : port
#
# ## Detailed Description:
# Calculates DMRS portfolio allocation
# Portfolios are sorted on BEME, OP or INV X ME X loading;
#
# ** Portfolio formation:
# Stocks are first independently sorted on ncharport (DMRS 2020 ncharport=3) portfolios for each characteristic
# in accordance to breakpoints.
# Each characteristic bucket is then sorted in nloadport (DMRS 2020 nloadport=3) portfolios based on the pre-formation loading.
# Notice, that the sorting on the loading is conditional on the characteristic sort.
"""

# %% Packages
import pandas as pd
import numpy as np
import sqlalchemy as sa
from itertools import chain

import user_setup as uset
from portools.find_breakpoints import find_breakpoints
from portools.sort_portfolios import sort_portfolios

desired_width = 10
pd.set_option('display.width', desired_width)
idx = pd.IndexSlice


def main(user):
    print('Started portfolio allocation.')
    # %% Parameters
    # User's parameters
    # -----------------
    mysql_vars = uset.user_setup(user)

    # Test portfolio parameters
    # --------------------------
    # Number of independent sorts (sorts on characteristics)
    ncharport = 3
    # Number of conditional sorts (sorts on pre-formation loadings)
    nloadport = 3

    # Characteristics/Loadings
    # ------------------------
    # * The first characteristic is used in all sorts.
    # * Usually this characteristic is MEsum_june.
    chars = {'MktRF': ['MEsum_june', 'BEME', 'OP', 'INV'],
             'SMB': ['MEsum_june', 'BEME', 'OP', 'INV'],
             'HML': ['MEsum_june', 'BEME'],
             'RMW': ['MEsum_june', 'OP'],
             'CMA': ['MEsum_june', 'INV']}

    loadings = {'MktRF': 'b',
                'SMB': 's',
                'HML': 'h',
                'RMW': 'r',
                'CMA': 'c'}

    # Additional characteristics
    # * For example, variables needed for filtering
    additional_chars = ['MEsum_dec', 'exchcd', 'MEjune']

    # %% Set MySQL Connection
    conn = sa.create_engine("mysql+pymysql://"+mysql_vars['SQL_USER'] +
                            ":" + mysql_vars['SQL_PASSWORD'] +
                            "@" + mysql_vars['SQL_HOST'] +
                            ":" + mysql_vars['SQL_PORT'] + "/dmrs")

    # %% Set Up
    # Breakpoints
    bpoints = [None] * len(chars.keys())
    bpoints = dict(zip(chars.keys(), bpoints))

    # Portfolio Allocation
    port = [None] * len(chars.keys())
    port = dict(zip(chars.keys(), port))

    # Create breakpoints dictionaries
    qchar = np.arange(0, 1, 1 / ncharport)[1:ncharport]
    qload = np.arange(0, 1, 1 / nloadport)[1:nloadport]

    # --------------------------------------------------------------------------------------#
    # %% Download data
    # * Characteristics data
    # ** download characteristics data for all stocks with share code (10 or 11) and traded in NYSE, Nasdaq or Amex.
    all_vars = list(np.unique([x for v in chars.values() for x in v])) + additional_chars
    cdata = pd.read_sql_query("""SELECT permno, rankyear, %s FROM wrds.stock_annual 
                                        WHERE shrcd IN (10, 11) 
                                        AND exchcd IN (1, 2, 3);"""
                              % ','.join(all_vars),
                              conn)
    del all_vars

    # * Loading data
    all_loads = list(np.unique([x for v in loadings.values() for x in v]))
    ldata = pd.read_sql_query("""SELECT permno, ranktime as rankyear, %s FROM beta.betas
                                        WHERE beta_id = 2;"""
                              % ','.join(all_loads),
                              conn)
    del all_loads
    print('PA: Finished downloading the data.')

    # %% Breakpoints and Portfolio Allocation
    for cp in chars.keys():

        # --------------------------------------------------------------------------------------#
        # %% Quantiles
        sort_cquantiles = {i: qchar for i in chars[cp]}
        sort_lquantiles = {i: qload for i in loadings[cp]}

        # --------------------------------------------------------------------------------------#
        # %% Characteristics Sorts
        # * Define the break point filters and sample filters
        bp_filters = [None] * len(chars[cp])
        bp_filters = dict(zip(chars[cp], bp_filters))

        sample_filters = [None] * len(chars[cp])
        sample_filters = dict(zip(chars[cp], sample_filters))

        for sortvar in chars[cp]:
            # notice that the way we defined beme or beme is null if be<=0
            bp_filters[sortvar] = ((cdata[sortvar].notnull()) &
                                   (cdata['exchcd'].isin([1])))
            # sample_filters[sortvar] = ((cdata.MEsum_june > 0) & (cdata.MEsum_dec > 0) & (cdata[sortvar].notnull()))
            sample_filters[sortvar] = ((cdata.MEsum_june > 0) &
                                       (cdata.MEsum_june.notnull()) &
                                       (cdata.MEjune > 0) &
                                       (cdata.MEjune.notnull()) &
                                       (cdata[sortvar].notnull())
                                       )

        # Sorts
        # ** Find Breakpoints
        # The number of firms Ken French reports for each characteristic sort varies. This means his universe of stocks
        # for sorting changes across characteristics. We follow this method.
        # That is why we need to calculate breakpoints separately.
        breakpoints = [None] * len(chars[cp])
        breakpoints = dict(zip(chars[cp], breakpoints))

        for sortvar in chars[cp]:
            breakpoints[sortvar] = find_breakpoints(data=cdata[bp_filters[sortvar]],
                                                    quantiles={sortvar: sort_cquantiles[sortvar]},
                                                    id_variables=['rankyear', 'permno']
                                                    )

        # ** Portfolio Sorts
        portsorts = [None] * len(chars[cp])
        portsorts = dict(zip(chars[cp], portsorts))

        for sortvar in chars[cp]:
            portsorts[sortvar] = sort_portfolios(data=cdata[sample_filters[sortvar]],
                                                 quantiles={sortvar: sort_cquantiles[sortvar]},
                                                 id_variables=['rankyear', 'permno', 'exchcd'],
                                                 breakpoints={sortvar: breakpoints[sortvar]}
                                                 )

        # ** Merge all separate breakpoints and portfolio allocations together
        breakpoints['MEsum_june'].columns = ['rankyear', 'MEsum_june1', 'MEsum_june2']
        bpoints[cp] = breakpoints['MEsum_june'].copy()
        for sortvar in chars[cp][1:]:
            breakpoints[sortvar].columns = ['rankyear']+[sortvar+str(i) for i in range(1, ncharport)]
            # Notice that this is an outer join
            bpoints[cp] = bpoints[cp].merge(breakpoints[sortvar], on=['rankyear'], how='outer')
        del breakpoints

        port[cp] = portsorts['MEsum_june'].copy()
        for sortvar in chars[cp][1:]:
            # Notice that this is an outer join
            port[cp] = port[cp].merge(portsorts[sortvar], on=['permno', 'rankyear'], how='outer')
        del portsorts

        # Drop nas
        port[cp].dropna(how='any', inplace=True)
        del bp_filters, sample_filters
        print('PA: Finished sorting on characteristics for %s.' % cp)

        # --------------------------------------------------------------------------------------#
        # %% Loading Sorts
        portvars = [i+'portfolio' for i in chars[cp]]
        ldatavar = pd.merge(port[cp], ldata[['permno', 'rankyear', loadings[cp]]], how='inner')
        ldatavar.dropna(inplace=True)

        # Create list of cp interactions
        for i in range(len(portvars)-1):
            temp = ldatavar.groupby([portvars[0], portvars[i+1]]).apply(sort_portfolios,
                                                                        quantiles=sort_lquantiles,
                                                                        id_variables=['rankyear', 'permno'],
                                                                        silent=True)
            temp.reset_index(drop=True, inplace=True)
            temp.rename(columns={loadings[cp]+'portfolio': loadings[cp]+portvars[i+1]}, inplace=True)
            # Add to portfolio allocation
            port[cp] = pd.merge(port[cp], temp, on=['rankyear', 'permno'])

        print('PA: Finished sorting on loadings for %s.' % cp)
        del sort_cquantiles, sort_lquantiles, portvars, ldatavar, temp

    del ncharport, nloadport, cdata, ldata
    print('Finished portfolio allocation.')

    return port


if __name__ == "__main__":
    user = 'Lira'
    main(user=user)
