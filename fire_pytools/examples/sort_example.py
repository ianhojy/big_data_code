import pandas as pd
import numpy as np

from portools.find_breakpoints import *
from portools.sort_portfolios import *

adata = pd.DataFrame({'permno': [1001, 2002]*5,
                      'rankyear': [2000]*2 + [2001]*2 + [2002]*2 + [2003]*2 + [2004]*2,
                      'exch_cd': 1,
                      'me': np.linspace(1, 10, 10),
                      'beme': np.linspace(0.10, 2, 10)})

# %% Calculate Breakpoints
bp_me = find_breakpoints(data=adata,
                         quantiles={'me': [0.5]},
                         id_variables=['rankyear', 'permno', 'exch_cd'])


bp_beme = find_breakpoints(data=adata,
                           quantiles={'beme': [0.5]},
                           id_variables=['rankyear', 'permno', 'exch_cd'])

# %% Sort Portfolios
port = sort_portfolios(data=adata,
                       quantiles={'me': [0.5], 'beme': [0.5]},
                       id_variables=['rankyear', 'permno', 'exch_cd'])

