#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Date: 2019-02
Code:
    Import CRSP Stock File tables (SF). Merge with Stock Events (SE) for static characteristics.

------

Dependence:
utils/check_primary_key_integrity

------

Cheat Sheet for WRDS query submission:
--------------------------------------
db.describe_table(library='compm', table='aco_pnfnda')
db.get_table()
db.list_tables(library='compm')
db.engine()
db.insp()
db.raw_sql()
db.get_row_count()
db.list_libraries()
db.schema_perm()

"""

# %% Packages
import wrds
import warnings
import pandas as pd
import numpy as np
import time
from utils.pk_integrity import *



# %% Auxiliary

# TABLE is used inside crsp_sf
"""
Define variable source table (' msf', 'mseall', 'dsf', 'dsfall'). Daily tables are not yet implemented.
'sf' and 'seall' indicate that variable is available in both dsf or dseall and msf and mseall.
Variables in 'all' are available in all four tables, and the union of sf and seall values are returned.
"""
# TODO: conflicts shouldn't exist but check anyways for future proofing.

TABLE = {'cusip': 'all',
         'permco': 'all',
         'issuno': 'all',
         'hexcd': 'all',
         'hsiccd': 'all',
         'shrout': 'all',
         'bidlo': 'sf',
         'askhi': 'sf',
         'prc': 'sf',
         'vol': 'sf',
         'ret': 'sf',
         'bid': 'sf',
         'ask': 'sf',
         'cfacpr': 'sf',
         'cfacshr': 'sf',
         'retx': 'sf',
         'altprc': 'msf',
         'spread': 'msf',
         'altprcdt': 'msf',
         'openprc': 'dsf',
         'numtrd': 'dsf',
         'comnam': 'seall',
         'dclrdt': 'seall',
         'dlamt': 'seall',
         'dlpdt': 'seall',
         'dlstcd': 'seall',
         'ncusip': 'seall',
         'nextdt': 'seall',
         'paydt': 'seall',
         'rcrddt': 'seall',
         'shrcls': 'seall',
         'shrflg': 'seall',
         'ticker': 'seall',
         'hsicmg': 'seall',
         'hsicig': 'seall',
         'nameendt': 'seall',
         'shrcd': 'seall',
         'exchcd': 'seall',
         'siccd': 'seall',
         'tsymbol': 'seall',
         'naics': 'seall',
         'primexch': 'seall',
         'trdstat': 'seall',
         'secstat': 'seall',
         'distcd': 'seall',
         'divamt': 'seall',
         'facpr': 'seall',
         'facshr': 'seall',
         'acperm': 'seall',
         'accomp': 'seall',
         'shrenddt': 'seall',
         'nwperm': 'seall',
         'nwcomp': 'seall',
         'dlretx': 'seall',
         'dlprc': 'seall',
         'dlret': 'seall',
         'trtscd': 'seall',
         'trtsendt': 'seall',
         'nmsind': 'seall',
         'mmcnt': 'seall',
         'nsdinx': 'seall',
         'year': 'mseall',
         'month': 'mseall'}

# MSEPAD is used when creating CRSP
# When merging mse and sf tables and for vars in MSEPAD we should copy information from
# event observations to succeeding non-event observations.
MSEPAD = ["ticker", "comnam", "ncusip", "shrout", "siccd", "exchcd",
          "shrcls", "shrcd",  "shrflg", "trtscd", "nmsind", "mmcnt",
          "naics", "nsdinx"]


# %% Function Definition


def crsp_sf(varlist, start_date, end_date, freq, permno_list=None, shrcd_list=None, exchcd_list=None, db=None):
    """
    Download CRSP annual update stock data (daily or monthly).

    Primary key is permno-date.

    Code inspired by WRDS msf/mse merge SAS code.
    https://wrds-www.wharton.upenn.edu/pages/support/research-wrds/sample-programs/extract-daily-stock-file-and-events-data/?_ga=2.111265728.590111789.1528841242-1500651139.1501534854

    Parameters
    ----------
    varlist: list
        List of CRSP variables to be queried. See CRSP documentation for available variables.
        Technical note: like WRDS website, user does not need to know whether variable comes from
        msf (dsf) or mseall (dseall) tables. Index returns are not available through this method.
    start_date: string
        Sample start date (e.g. '1930-01-01').
    end_year: string
        Sample end date (e.g. '2017-12-31').
    freq: string
        Data frequency. Must be 'daily' or 'monthly'. First option queries CRSP's Daily Stock Files (dsf and dseall).
        Second one queries CRSP's Monthly Stock Files (msf and mseall).
    permno_list: list, optional
        List of permno's to be included in the output table. If None, include all.
    shrcd_list: list, optional
        List of CRSP share codes to be included in the output table. Typical value is [10, 11], which includes common
        stocks only. If None, include all.
    exchcd_list: list, optional
        List of CRSP exchange codes to be included in the output table. Typical value is [1, 2, 3], which includes
        NYSE, NASDAQ or AMEX listed securities. If None, include all.
    db: wrds connect object, optional

    Examples
    --------
    These replications should return the same number of rows as WRDS's web query for
    CRSP annual update daily and monthly stock files with no date or company code restrictions:

    df = download(['ret'], '1900-01-01', datetime.date.today().strftime("%Y-%m-%d"), '
                  daily', permno_list = [14593, 10107], shrcd_list = [10, 11], exchcd_list = [1, 2, 3])

    df = download(['ret'], '2015-01-01', datetime.date.today().strftime("%Y-%m-%d"), 'monthly')

    """

    start_time = time.time()
    if db is None:
        db = wrds.Connection(wrds_username='lmota') # make sure to configure wrds connector before hand.

    assert freq in ['daily', 'monthly'], "freq must be either 'daily' or 'monthly'."

    if freq == 'daily' and len(varlist) > 1:
        warnings.warn('''Querying multiple variables from daily tables may take a long time if limited RAM is available (16GB or less).
                         WRDS connection may time out causing an error.''')

    prefix = 'd' if freq == 'daily' else 'm'

    sfvars = ['permno', 'date']
    sfvars += [i for i in varlist if TABLE[i] in ['{}sf'.format(prefix), 'sf']]
    sfvars += [i + ' AS {}_sf'.format(i) for i in varlist if TABLE[i] == 'all']

    seallvars = ['permno', 'date']

    if ('divamt' in set(varlist)) & ('facpr' not in set(varlist)):
        seallvars += ['facpr']
    if ('divamt' in set(varlist)) & ('distcd' not in set(varlist)):
        seallvars += ['distcd']

    seallvars += [i for i in varlist if TABLE[i] in ['{}seall'.format(prefix), 'seall']]
    seallvars += [i + ' AS {}_seall'.format(i) for i in varlist if TABLE[i] == 'all']

    permnos = 'AND permno IN ({})'.format(", ".join(map(str, permno_list))) if permno_list else ''

    sql = '''
          SELECT {}
          FROM crspq.{}sf
          WHERE date >= DATE '{}'
          AND date <= DATE '{}'
          {}
          '''.format(', '.join(sfvars), prefix, start_date, end_date, permnos)

    df = db.raw_sql(sql, date_cols=['date'])

    sql = '''
          SELECT {}
          FROM crspq.{}seall
          WHERE date >= DATE '{}'
          AND date <= DATE '{}'
          {}
          '''.format(', '.join(seallvars), prefix, start_date, end_date, permnos)

    df2 = db.raw_sql(sql, date_cols=['date'])

    # There are some duplicates in mseall that differ in variables related to cash distributions and acquisitions.
    # Cumulate if possible, or remove arbitrary row. Raise warning if any relevant variable was queried.
    nonunique_cols = ['rcrddt', 'dclrdt', 'acperm', 'accomp', 'paydt', 'distcd', 'facpr', 'facshr']

    if set(varlist).intersection(set(nonunique_cols)):
        warnings.warn('''Duplicate permno-date observations were deleted arbitrarily.
                             {} percent of the observations.
                             Please resolve manually the affected columns:
                             {} 
                             '''.format(str((df2.duplicated(subset=['permno', 'date']).sum() / df2.shape[0]) * 100),
                                        ', '.join(nonunique_cols)))

    # Duplicated happens when there is two distribution types at the same day.
    # We need to have permno/date as primary key
    # df2.duplicated(subset=['permno', 'date', 'distcd', 'paydt']).sum() # TODO: why isn't this primary key?
    # We start with  52665 duplicated permno/date. 1867 non zero facpr (monthly).

    if 'divamt' in set(varlist):
        # Calculate (TOTAL) DIVAMT:
        # "If the Distribution code is 6225, a nonzero amount represents an offer price given to a certain amount of
        # shares. For these cases, the dollar value per share is actually DIVAMT multiplied by the percent of shares
        # accepted by the offer, where the percent of shares accepted can be derived by multiplying FACPR by negative
        # one. See FACPR." (WRDS)
        # print(df2[df2.distcd == 6225].to_string())

        df2.set_index(['permno', 'date'], inplace=True)

        div = df2[['distcd', 'divamt', 'facpr']].copy()
        div.reset_index(inplace=True)
        div.loc[div.distcd == 6225, 'divamt'] = div['facpr']*(-1)*div['divamt']

        div = div.groupby(['permno', 'date'])['divamt'].sum(min_count=1).to_frame('divamt')

        df2.reset_index(inplace=True)
        df2.drop(columns=['divamt'], inplace=True)

    # For all other entries keep last
    df2 = df2[~df2.duplicated(subset=['permno', 'date'], keep='last')].copy()
    pk_integrity(df2, ['date', 'permno'])

    # Merge tables.
    df['permno'] = df['permno'].astype('int')
    df2['permno'] = df2['permno'].astype('int')

    # Merge tables with following logic: for each sf observation, merge most recent seall observation with same permno.
    # If particular permno-date combination does not exist in sf but does in seall, include that observation.
    df.sort_values(['date', 'permno'], inplace=True)
    df2.sort_values(['date', 'permno'], inplace=True)

    df = pd.merge_asof(df, df2, on='date', by='permno', direction='backward')

    df2 = df2[(df2['date'] >= start_date) & (df2['date'] <= end_date)]
    df = df.merge(df2, on=['permno', 'date'], how='outer', validate='1:1', suffixes=('', '_y'), indicator=True)

    for i in df2.columns.tolist()[2:]:
        df.loc[df['_merge'] == 'right_only', i] = df[i + '_y']
    df.drop([s + '_y' for s in df2.columns.tolist()[2:]] + ['_merge'], axis=1, inplace=True)
    del df2

    # Make sure that downloaded columns that exist in both tables agree and keep union of them.
    for i in [j for j in varlist if TABLE[j] == 'all']:
        mask = df[[i + '_sf', i + '_seall']].notnull().all(axis=1)
        assert (df.loc[mask, i + '_sf'] == df.loc[mask, i + '_seall']).all(), \
            'sf and seall had conflicting values for {}.'.format(i)
        df.rename(columns={i + '_sf': i}, inplace=True)
        df.loc[df[i].isnull(), i] = df[i + '_seall']
        df.drop(i + '_seall', axis=1, inplace=True)

    # Add total divamt
    if 'divamt' in set(varlist):
        df = df.merge(div, on=['permno', 'date'], how='left')

    # delete wanted rows
    if exchcd_list:
        df = df[df.exchcd.isin(exchcd_list)]
    if shrcd_list:
        df = df[df.shrcd.isin(shrcd_list)]
    df = df[(df['date'] >= start_date) & (df['date'] <= end_date)]

    df = df[['permno', 'date']+varlist]

    pk_integrity(df, ['permno', 'date'])

    df.sort_values(['permno', 'date'], inplace=True)

    print("CRSP data was successfully downloaded in %s seconds." % str(time.time() - start_time))

    return df