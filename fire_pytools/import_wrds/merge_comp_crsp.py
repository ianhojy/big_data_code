#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2018-06
Code:
    Merge CRSP and Compustat using ccmxpf_lnkhist link tab;e.

Cheat Sheet for WRDS query submission:
--------------------------------------
db.describe_table(library='compm', table='aco_pnfnda')
db.get_table()
db.list_tables(library='compm')
db.engine()
db.insp()
db.raw_sql()
db.get_row_count()
db.list_libraries()
db.schema_perm()
"""

# %% Packages
import wrds
import pandas as pd

from utils.pk_integrity import *

# %% Function Definition


def merge_compustat_crsp(comp_data, db=None):
    """
    Query CRSP/Compustat merged table (annual or quarterly).

    Output table is CRSP-centric with permno-datadate being the primary key.
    Fiscal period end date must be within link date range.
    Quarterly table is a calendar view (in contrast to a fiscal view).

    Replicates the functionality of WRDS's SAS scripts ccmfundq.sas and ccmfunda.sas:
    https://wrds-web.wharton.upenn.edu/wrds/support/Data/_003Sample%20Programs/CRSP/index.cfm?

    To exactly replicate quarterly script, remove "AND datacqtr IS NOT NULL" and use
    "WHERE linktype IN ('LU', 'LC', 'LD', 'LF', 'LN', 'LS')" in sql query.
    These changes were made to make output table unique in permno-datadate (drops ~ 0.35% of observations).


    Parameters
    ----------
    comp_data: pandas data-frame
        compustat data.
        [gvkey, datadate] as primary key
    db: wrds connection

    Examples
    --------
    df = merge_compustat_crsp(comp_data)

    """

    if db is None:
        db = wrds.Connection(wrds_username='lmota')  # make sure to configure wrds connector before hand.

    sql = '''SELECT gvkey, lpermno as permno, lpermco as permco, linkdt, linkenddt
             FROM crspq.ccmxpf_lnkhist
             WHERE linktype IN ('LC', 'LU', 'LS') 
          '''

    link_table = db.raw_sql(sql, date_cols=['linkdt', 'linkenddt'])

    # link_table[link_table.permco==54311]

    df = pd.merge(comp_data, link_table, on=['gvkey'], how='inner')

    df = df[(df.datadate >= df.linkdt) | (df.linkdt.isnull())]
    df = df[(df.datadate <= df.linkenddt) | (df.linkenddt.isnull())]

    df['permno'] = df['permno'].astype(int)
    df['permco'] = df['permco'].astype(int)
    df['datadate'] = pd.to_datetime(df['datadate'])

    pk_integrity(df, ['permno', 'datadate'])

    return df
