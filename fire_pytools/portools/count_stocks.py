#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python
# coding: utf-8

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
"""

# %% Packages
import pandas as pd

# %% Function definition


def count_stocks(data, var):
    """
     Count stocks for each combination in var.

    Parameters:
    ----------
    data: DataFrame
        Should include all data
    var: list
        List of variables to create bins.

    Return:
    -------
    DataFrame with stocks counts
    """
    return pd.crosstab(data[var[0]], data[var[1]])
