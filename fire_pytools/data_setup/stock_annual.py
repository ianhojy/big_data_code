#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Date: 2019-02
Code:
    Creates stock_annual pandas data frame.
    Import Compustat, PRBA, create variables and merges with CRSP.

Notes:
------
Calculates:
    Book-equity (BE),
    Operating profits (OP) and Operating profitability (OP/BE),
    Investment (INV)
    Net Equity Issuance (NEI)
    Net Debt Issuance (NDI)
    Leverage

"""

"""
Compustat XpressFeed Variables:
# * AQC(Y)  = Acquisitions
# * AT      = Total Assets  
# * CAPX    = Capital Expenditures  
# * CEQ     = Common/Ordinary Equity - Total 
# * CH      = Cash
# * CHE     = Cash and Short-Term Investments
# * COGS    = Cost of Goods Sold
# * CSHO    = Common Shares Outstanding
# * DLC     = Debt in Current Liabilities 
# * DLCCH   = Current Debt - Changes
# * DLTIS   = Long-Term Debt - Issuance
# * DLTT    = Long-Term Debt - Total
# * DLTR    = Long-Term Debt - Reduction
# * DP      = Depreciation and Amortization
# * DVC     = Divdends common/ordinary  
# * DVP     = Dividends - Preferred/Preference
# * EBIT    = Earnings Before Interest and Taxes
#             This item is the sum of Sales - Net (SALE) minus Cost of Goods Sold (COGS) 
#             minus Selling, General & Administrative Expense (XSGA) minus Depreciation/Amortization (DP). 
# * EBITDA  = Earnings Before Interest
#             This item is the sum of Sales - Net (SALE) minus Cost of Goods Sold (COGS) 
#             minus Selling, General & Administrative Expense (XSGA).
# * GP      = Gross Profits
# * IB      = Income Before Extraordinary Items
# * ICAPT   = Invested Capital - Total
# * ITCB    = Investment Tax Credit (Balance Sheet)  
# * LT      = Total Liabilities        
# * MIB     = Minority Interest (Balance Sheet) 
# * NAICS   = North American Industrial Classification System Variable Name
# * NAICSH  = North American Industry Classification Codes - Historical Company Variable Name
# * NI      = Net Income
# * OIBDPQ  = Operating Income Before Depreciation
# * PPEGT   = "Property, Plant and Equipment - Total (Gross)"
# * PRBA    = Postretirement Benefit Assets (from separate pension annual file) 
# * PRSTKC  = Purchase of Common and Preferred Stock                   
# * PRSTKCC = Purchase of Common Stock (Cash Flow)                   
# * PSTKRV  = Preferred Stock Redemption Value   
# * PSTK    = Preferred/Preference Stock (Capital) - Total (kd: = par?)               
# * PSTKL   = Preferred Stock Liquidating Value     
# * PSTKRV  = Preferred Stock Liquidating Value          
# * RE      = Retained Earnings
# * REVT    = Revenue - Total
# * RSTCHE  = Restricted Cash & Investments - Current
# * RSTCHELT = Long-Term Restricted Cash & Investments
# * SALE    = Sales/Turnover Net
# * SEQ     = Shareholders Equity   
# * SIC     = Standard Industrial Classification Code 
# * SSTK    = Sale of Common and Preferred Stock 
# * TXDB    = Deferred Taxes Balance Sheet
# * TXDI    = Income Taxes - Deferred
# * TXDITC  = Deferred Taxes and Investment Tax Credit                        
# * WCAPCH  = Working Capital Change - Total
# * XINT    = Interest and Related Expense - Total 
# * XLR     = Staff Expense - Total
# * XRD     = Research and Development Expense 
# * XSGAQ   = Selling, General and Administrative Expenses (millions) 
"""

# %% Packages
import wrds
import pandas as pd
import numpy as np
import datetime
from pandas.tseries.offsets import MonthEnd

from utils.pk_integrity import *
from utils.post_event_nan import *

from import_wrds.compd_fund import *
from import_wrds.compd_aco_pnfnd import *
from import_wrds.crsp_sf import *
from import_wrds.merge_comp_crsp import *
from import_kf.dff_be import *

# %% Functions

def calculate_be(df):
    """
    BE = Share Equity(se) - Prefered Stocks(ps) + Deferred Taxes(dt) - Post retirement Benefit Assets(prba)

    Parameters
    ----------
    df: data frame
        Compustat table with columns ['seq', 'ceq', 'at', 'lt', 'mib', 'pstkrv', 'pstkl', 'pstk', 'txditc', 'txdb', 'itcb', 'prba']

    Definition:
    -----------
    BE is the stockholders book equity, plus balance sheet deferred taxes and investment tax credit (if available),  minus the book value
    of preferred stock. Depending on availability, we use redemption, liquidation, or par value (in that order) for the book value of preferred stock.
    Stockholders equity is the value reported by Moody or COMPUSTAT, if it is available.
    If not, we measure stockholders equity as the book value of common equity plus the par value of preferred stock,
    or the book value of assets minus total liabilities (in that order)".* DFF, JF, 2000, pg 393.
    This is the definition posted on Ken French website:

    """

    required_cols = ['fyear', 'seq', 'ceq', 'at', 'lt', 'mib', 'pstkrv', 'pstkl', 'pstk', 'txditc', 'txdb', 'itcb', 'prba']

    assert set(required_cols).issubset(df.columns), 'Following funda dataitems needed: {}'.format(required_cols)

    df = df[required_cols].copy()

    # Shareholder Equity
    df['se'] = df['seq']

    # Uses Common Equity (ceq) + Preferred Stock (pstk) if SEQ is missing:
    df['se'].fillna((df['ceq'] + df['pstk']), inplace=True)

    # Uses Total Assets (at) - Liabilities (lt) + Minority Interest (mib, if available), if others are missing
    df['se'].fillna((df['at'] - df['lt'] + df['mib'].fillna(0)), inplace=True)

    # Preferred Stock
    # Preferred Stock (Redemption Value)
    df['ps'] = df['pstkrv']
    # Uses Preferred Stock (Liquidating Value (pstkl)) if Preferred Stock (Redemption Value) is missing
    df['ps'].fillna(df['pstkl'], inplace=True)
    # Uses Preferred Stock (Carrying Value (pstk)) if others are missing
    df['ps'].fillna(df['pstk'], inplace=True)

    # Deferred Taxes
    # Uses Deferred Taxes and Investment Tax Credit (txditc)
    df['dt'] = df['txditc']

    # This was Novy-Marx old legacy code. We drop this part to be in accordance with Ken French.
    # Uses Deferred Taxes and Investment Tax Credit(txdb) + Investment Tax Credit (Balance Sheet) (itcb) if txditc is missing

    df['dt'].fillna((df['txdb'].fillna(0) + df['itcb'].fillna(0)), inplace=True)
    # If all measures are missing, set dt to missing
    df.loc[pd.isnull(df['txditc']) & pd.isnull(df['txdb']) & pd.isnull(df['itcb']), 'dt'] = np.nan

    df.loc[df['fyear'] >= 1993, 'dt'] = 0

    # Book Equity
    # Book Equity (BE) = Share Equity (se) - Prefered Stocks (ps) + Deferred Taxes (dt)
    BE = (df['se']  # shareholder equity must be available, otherwise BE is missing
          - df['ps']  # preferred stock must be available, otherwise BE is missing
          + df['dt'].fillna(0)  # add deferred taxes if available
          - df['prba'].fillna(0))  # subtract postretirement benefit assets if available

    return BE


def calculate_op(df):
    """
    Operating Profitability (OP)
    Revenues (SALE (? not sure)) minus cost of goods sold (ITEM 41/COGS), minus selling, general, and administrative expenses (ITEM 189/XSGA),
    minus interest expense (ITEM 15/XINT (? Not Sure)) all divided by book equity.

    Fama, French (2015, JFE, pg.3)
    Ken Frech's website:
    The portfolios for July of year t to June of t+1 include all NYSE, AMEX, and NASDAQ stocks for which we have ME for December of t-1 and June of t,
    (positive) BE for t-1, non-missing revenues data for t-1, and non-missing data for at least one of the following: cost of goods sold, selling,
    general and administrative expenses, or interest expense for t-1.
    """
    required_cols = ['sale', 'cogs', 'xsga', 'xint', 'be']

    assert set(required_cols).issubset(df.columns), 'Following funda dataitems needed: {}'.format(required_cols)

    df = df[required_cols].copy()

    df['cost'] = df[['cogs', 'xsga', 'xint']].sum(axis=1, skipna=True)
    df.loc[df[['cogs', 'xsga', 'xint']].isnull().all(axis=1), 'cost'] = np.nan

    df['op'] = df['sale'] - df['cost']
    df.loc[(df.be > 0), 'opbe'] = df['op'] / df['be']

    return df['op'], df['opbe']


def calculate_inv(df, pk=['gvkey', 'fyear']):
    """
    Investment (INV)
    The change in total assets from the fiscal year ending in year t-2 to the fiscal year ending in t-1, divided by t-2 total assets.
    Fama, French (2015, JFE, pg.3)

    Notes:
    ------
    Ken Frech's website:
    The portfolios for July of year t to June of t+1 include all NYSE, AMEX, and NASDAQ stocks for which we have market equity data for June of t and total assets
    data for t-2 and t-1.
    """

    required_cols = pk + ['datadate', 'at']

    assert set(required_cols).issubset(df.columns), 'Following funda dataitems needed: {}'.format(required_cols)

    df = df[required_cols].copy()

    if any(df.fyear.isnull() & df['at'].notnull()):
        warnings.warn('''Missing fyear with valid at value. Row was arbitrarily deleted. ''')

    # Notice that [gvkey, fyear] is not a primary key for compustat.
    # There are some cases in which there are 2 datadate for the same fyear.
    # In many cases there is one of them have missing.  In this case we keep the entry that is not missing.
    df = df[df.fyear.notnull()]
    df = df[df['at'] > 0]

    df.sort_values(pk, inplace=True)
    df['lag_at'] = df.groupby(pk[0]).at.shift(1)
    df['inv'] = (df['at'] - df['lag_at']) / df['lag_at']

    # Take care if there are years missing
    df['fdiff'] = df.groupby(pk[0]).fyear.diff()
    df.loc[df.fdiff > 1, 'inv'] = np.nan

    # df[df['gvkey'] == '018073']

    return df['inv']


# %% Main Function

def main(varlist=['cusip', 'tic', 'cik', 'conm', 'fyear', 'fyr', 'aqc', 'at', 'capx', 'ceq', 'ch', 'che', 'cogs',
                  'csho', 'dlc', 'dlcch', 'dltis', 'dltt', 'dltr', 'dp', 'dv', 'dvc', 'dvp', 'ebit', 'ebitda',
                  'ib', 'icapt', 'itcb', 'ivao', 'lt', 'mib', 'naicsh', 'ni', 'oibdp', 'ppegt', 'prstkc', 'prstkcc',
                  'pstk', 'pstkl', 'pstkrv',  're', 'rstche', 'rstchelt', 'sale', 'seq', 'sppe', 'sich', 'sstk', 'txdb',
                  'txdi', 'txditc', 'wcapch', 'xint', 'xrd', 'xsga'],
         complete_table=True):

    print("Stock annual calculation started.")
    # %% Set Up
    db = wrds.Connection(wrds_username='lmota')  # make sure to configure wrds connector before hand.

    start_time = time.time()

    # %% Import Data
    start_date = '1950-01-01'  # '2017-01-01'#
    end_date = datetime.date.today().strftime("%Y-%m-%d")
    freq = 'annual'

    # Download firms fundamentals from Compustat.
    comp_data = compd_fund(varlist=varlist, start_date=start_date, end_date=end_date, freq='annual', db=db)

    # Download pension data
    varlist_aco = ['prba']
    pension_data = compd_aco_pnfnd(varlist=varlist_aco, start_date=start_date, end_date=end_date, freq=freq, db=db)

    del varlist_aco

    # Download Davis, Fama, French BE Data
    dff = dff_be()

    # CRSP ME Data
    varlist_crsp = ['exchcd', 'naics', 'permco', 'prc', 'shrcd', 'shrout', 'siccd', 'ticker']
    start_date = '1925-01-01'  # '2017-01-01' #
    end_date = datetime.date.today().strftime("%Y-%m-%d")
    freq = 'monthly'  # 'daily'
    crspm = crsp_sf(varlist_crsp,
                    start_date,
                    end_date,
                    freq=freq,
                    db=db)

    del varlist_crsp

    # Import names table (used to add PERMCO to DFF be data)
    snames = db.raw_sql('SELECT permno, permco FROM crspq.stocknames')
    snames.drop_duplicates(inplace=True)
    # snames.groupby('permno').count().max()

    # Merge
    comp = pd.merge(comp_data, pension_data, on=['gvkey', 'datadate'], how='left')

    del (comp_data, pension_data)

    # %% Add variables

    # Add BE
    comp['be'] = calculate_be(comp)

    # Add OP: naming here is to be consistent with risk and returns projects.
    comp['op'], comp['opbe'] = calculate_op(comp)

    # Add INV
    comp['inv_gvkey'] = calculate_inv(comp)

    # Add rankyear
    comp['rankyear'] = comp['fyear'] + 1

    # Calculate extra variables
    if complete_table:
        # Net Equity Repurchase (Millions)
        comp['ner'] = comp['prstkc'].fillna(0) - comp['sstk'].fillna(0)
        # Net Equity Repurchase Inc. Dividend (Millions)
        comp['nert'] = comp['dv'].fillna(0) + comp['prstkc'].fillna(0) - comp['sstk'].fillna(0)

        # Net Debt Issuance (Millions)
        ## Problem with AAPL: missing dltr
        comp['ndi'] = comp['dltis'].fillna(0) - comp['dltr'].fillna(0)

        # Leverage measures
        comp.loc[comp['at'] > 0, 'fdat'] = (comp.loc[comp['at'] > 0, 'dltt'] + comp.loc[comp['at'] > 0, 'dlc'])/comp.loc[comp['at'] > 0, 'at']
        comp.loc[comp['at'] > 0, 'fdat_net'] = (comp.loc[comp['at'] > 0, 'dltt'] + comp.loc[comp['at'] > 0, 'dlc'] - comp.loc[comp['at'] > 0, 'che'])/comp.loc[comp['at'] > 0, 'at']
        comp.loc[comp['at'] > 0, 'ltat'] = comp.loc[comp['at'] > 0, 'lt']/comp.loc[comp['at'] > 0, 'at']
        comp.loc[comp['be'] > 0, 'fdbe'] = (comp.loc[comp['be'] > 0, 'dltt'] + comp.loc[comp['be'] > 0, 'dlc']) / comp.loc[comp['be'] > 0, 'be']
        comp.loc[comp['be'] > 0, 'fdbe_net'] = (comp.loc[comp['be'] > 0, 'dltt'] + comp.loc[comp['be'] > 0, 'dlc'] - comp.loc[comp['be'] > 0, 'che']) / comp.loc[comp['be'] > 0, 'be']

    # %% Take care of duplicates

    # There are 2 entries in which fyear is missing. All variables are null in these cases.
    # comp.loc[comp['fyear'].isnull(), ['gvkey', 'cusip', 'permno', 'fyear', 'at', 'be']]
    comp.dropna(subset=['fyear'], inplace=True)

    # Merged data set has [permno, datadate] as primary key. We need ['permno', 'fyear'] as primary key.
    # Six duplicated cases in which we choose the latest observation.
    # comp[comp[['permno', 'fyear']].duplicated(keep=False)][['permno', 'gvkey', 'datadate', 'fyear', 'at']]
    # comp[comp['permno']==22074][['permno', 'gvkey', 'datadate', 'fyear', 'at']]

    # These are cases where the gvkey changes (probably merger) and the fiscal-year-end (fyr) also changes
    # by keeping last, we put the merger investment into the year it happens
    # That seems reasonable
    comp.sort_values(by=['gvkey', 'fyear', 'at'], na_position='last')
    comp = comp[~comp.duplicated(subset=['gvkey', 'fyear'], keep='first')]

    # %% Add CRSP Variables

    # Link CRSP/Compustat table
    lcomp = merge_compustat_crsp(comp, db=db)
    print("CRSP and Compsuat merge created %d (fyear, permno) duplicates." %lcomp[lcomp.duplicated(subset=['permno', 'fyear'])].shape[0])
    print("Keeping only the last available datadate per PERMNO.")

    lcomp.sort_values(by=['permno', 'fyear', 'datadate'], inplace=True)
    lcomp = lcomp[~lcomp.duplicated(subset=['permno', 'fyear'], keep='last')]

    #  Calculate INV by PERMCO
    lcomp['inv_permco'] = calculate_inv(lcomp, pk=['permco', 'fyear'])

    # Add Davis data
    dff.rename(columns={'be': 'be_dff'}, inplace=True)
    # Add PERMCO to DFF data: Important for ME sum later
    dff = pd.merge(dff, snames, on=['permno'], how='left')

    print('There are %d PERMNOs without a valid PERMCO in DFF BE data: not present in the stock names table.' % dff[dff.permco.isnull()].permno.unique())

    lcomp = pd.merge(lcomp, dff, on=['permno', 'permco', 'rankyear'], how='outer')
    lcomp.be.fillna(lcomp['be_dff'], inplace=True)
    lcomp.drop(columns=['be_dff'], inplace=True)
    lcomp.fyear.fillna(lcomp['rankyear'] - 1, inplace=True)

    print('Number of not valid PERMCOs in lcomp: %d' % round(lcomp.permco.isnull().sum() / lcomp.shape[0], 4))

    del dff, snames
    # Notice that, since int does not support null, outer merge changes dtype of permco

    lcomp.sort_values(['permno', 'rankyear'], inplace=True)

    # ME, SICCD, TICKER, exchcd and shrcrd from June
    crspm['me'] = crspm.prc.abs()*crspm.shrout

    crspjune = crspm.loc[crspm.date.dt.month == 6, ['permno', 'permco', 'date', 'me', 'exchcd', 'shrcd', 'ticker', 'siccd']]
    crspjune['rankyear'] = crspjune.date.dt.year
    crspjune.drop('date', axis=1, inplace=True)
    crspjune.rename(columns={'permco': 'permcoc'}, inplace=True)
    stock_annual = lcomp.merge(crspjune, how='outer', on=['permno', 'rankyear'])
    stock_annual.loc[stock_annual.permco.isnull(), 'permco'] = stock_annual.loc[stock_annual.permco.isnull(), 'permcoc']
    stock_annual.drop(columns='permcoc', inplace=True)
    stock_annual.sort_values(['permno', 'rankyear'], inplace=True)

    # For summing size over issues of the same firm:
    # we rely on gvkey first
    # and if there is no gvkey (e.g. before lcompustat sample period) we use PERMCO
    stock_annual['gvkey_permco'] = stock_annual.gvkey.fillna(stock_annual['permco'])
    stock_annual['mesum_june'] = stock_annual.groupby(['fyear', 'gvkey_permco'])['me'].transform(np.sum, min_count=1)
    stock_annual.rename(columns={'me': 'mejune'}, inplace=True)
    del crspjune

    # Calculate ME december
    crspdec = crspm.loc[crspm.date.dt.month == 12, ['permno', 'date', 'me']]
    crspdec['fyear'] = crspdec.date.dt.year
    crspdec.drop('date', axis=1, inplace=True)
    crspdec.rename(columns={'me': 'medec'}, inplace=True)
    stock_annual = pd.merge(stock_annual, crspdec, how='left', on=['permno', 'fyear'])
    stock_annual['mesum_dec'] = stock_annual.groupby(['fyear', 'gvkey_permco'])['medec'].transform(np.sum, min_count=1)
    del crspdec, lcomp

    # Calculate book-to-market
    # In accordance with FF1993 and DFF2000 the BEME used to form portfolios in June of year t,
    # is BE for the fiscal year ending in t-1, divided by ME at December of t-1. ME for December
    stock_annual.loc[(stock_annual.be > 0) & (stock_annual.mesum_dec > 0), 'beme'] = stock_annual['be'] / stock_annual['mesum_dec']

    # Calculate again variables that depend on be values. Need to consider the DFF data OP
    stock_annual.loc[(stock_annual.be > 0), 'opbe'] = stock_annual['op'] / stock_annual['be']

    # Add CRSP SIC when missing
    stock_annual.sich.fillna(stock_annual['siccd'], inplace=True)

    # Back fill SIC code
    stock_annual['sich_filled'] = stock_annual.sich.copy()
    stock_annual['sich_filled'] = stock_annual.groupby('permno').sich_filled.fillna(method='bfill')
    stock_annual['sich_filled'] = stock_annual.groupby('permno').sich_filled.fillna(method='ffill')

    print('Number of entries with valis sich:')
    print(round(pd.isnull(stock_annual.sich).sum() / stock_annual.shape[0], 4))
    print('Number of entries with valis sich_filled:')
    print(round(pd.isnull(stock_annual.sich_filled).sum() / stock_annual.shape[0], 4))

    pk_integrity(stock_annual, ['permno', 'rankyear'])
    stock_annual.sort_values(['permno', 'rankyear'], inplace=True)

    stock_annual.drop(columns=['gvkey_permco'], inplace=True)

    print("Time to create stock_annual: %s seconds" % str(time.time() - start_time))

    return stock_annual


# %% Main
if __name__ == '__main__':
    main()